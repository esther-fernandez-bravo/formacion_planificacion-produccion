##Estructura de la carpeta Formaciones
* Nombre proyecto
  * Material formativo
    * Presentacion
    * Caso practico
      * Instalacion
      * Caso Introduccion
      * Caso Resolucion
      * Caso Discusion
  * Material profesor
    * Guión
    * Chuleta presentacion
  * Tareas 
    * Iniciales
    * Finales
    

## Algunas directrices sobre nombres de ficheros

* Para las notas comenzar el nombre con la fecha de la reunión llamada, ejemplo: 20170328\_ReunionLanzamiento.txt.
* Evitar poner espacios en los nombres de ficheros, usar \_, - o comenzar todas las palabras con mayúsculas (CamelCase).
* Evitar incluir carácteres raros: ? ¿ ¡ ! \ $ % &...

## Algunas ideas sobre qué agregar y qué no

### Directrices GENERALES sobre qué agregar y qué no agregar a un repositorio de git:

### NO agregar ficheros que:

1. Pesen más de dos MB.
1. Sean binarios (no sean texto) y tengan que ser actualizados muchas veces. Por ejemplo: {`.docx`, `.xlsx`};
1. Sean el resultado de una compilación. Por ejemplo: {".exe" si se tiene el código fuente, ".pdf" si se tiene el .tex}.
1. Sean generados por un proceso automático. Por ejemplo: {.err, .log, .aux}.
1. Tenga referencias absolutas a la máquina en la que se está trabajando. Por ejemplo: ficheros de configuración. Por ejemplo: {C:\Users\bob_lucia\Desktop\..}.
1. Tengan contraseñas. No es seguro!
1. Sean datos guardados en un caso binario o una base de datos. Por ejemplo: {".data" de AIMMS, ".Rdata" de R}.
1. Los archivos que no se agreguen al repositorio y deban ser compartidos se almacenarán en Drive o se compartirán mediante correo o slack. Slack no tiene histórico de archivos, por tanto, solo compartir por slack archivos con corta validez.

### SÍ agregar ficheros que:

1. A pesar de ser binarios, sean pequeños (\<100KB) e *imprescindibles* para la aplicación. Por ejemplo: imágenes, dll.
1. A pesar de ser ficheros para hacer pruebas (o de ejemplo), son pequeños (\<50KB), no son binarios y no van a cambiar *prácticamente nunca*.


## ¿Qué reviso cuando trabajo con...?

### AIMMS
Se suben los directorios y archivos señalados con +:

-backup: contiene backups de la aplicación. Pesan más de dos MB -> no se suben.

+data: contiene datos necesarios para ejecutar la aplicación que se almacenan en Drive. Para que exista esta carpeta en el repositorio se añadirá un README.txt.

+img: contiene las imágenes de la aplicación. Son imprescindibles para usar la aplicación -> se suben.

-log: registro de errores de AIMMS. Se generan mediante procesos automáticos -> no se suben.

+MainProject
 
  * +Pages: contiene las páginas de la interfaz. Imprescindibles -> se suben.
 
  * +Settings: contiene la configurtación de la herramienta (colores, fuentes..). Imprescindibles -> se suben.
 
  * +Tools: configuración de la herramienta. Imprescindible -> se sube.
 
  * +User Files/bitmaps: archivos imprescindibles de AIMMS -> se sube
 
+.ams: contiene el código desarrollado en AIMMS (procedimientos, parámetros..). Imprescindible -> se sube.

-.nch: mantiene el control de cambios de las sesiones de AIMMS. Los cambios realizados se guardan en .ams, por lo que no es necesario almacenar un histórico de cambios. Proceso automático -> no se sube.

+DataManagementSetup.xml: gestor de datos. Imprescindible -> se sube.

-Main.DeveloperState: guarda la configuración de las páginas. Es necesario para abrir la herramienta de forma correcta pero Bitbucket no lo reconoce y puede ser pesado. No se añade al repositorio pero se sube a Drive o se comparte por slack.

+MenuBuilder.xml: genera los menús creados. Imprescindible -> se sube.

+PageManager.xml: gestiona las páginas creadas. Imprescindible -> se sube.

+Project.xml: gestiona el proyecto. Imprescindible -> se sube.

+TemplateManager.xml: gestiona las plantillas creadas. Imprescindible -> se sube.

### Latex
Se suben los directorios y archivos señalados con +:

+codigo: almacena las capturas de código que puede incluir el documento. Suele contener archivos en formato .R o .py. Imprescindible -> se sube.

+imagenes: contiene imágenes que puede incluir el documento. Imprescindible -> se sube.

+Pages: contiene las páginas que puede incluir el documento.

    +.tex: contenido de las páginas. Imprescindible -> se sube.

    -.aux: Miktex genera este tipo de archivo de forma automática al compilar el .tex. Proceso automático -> no se sube.

+.sty: estilo del documento Latex. Si el estilo está disponible en CRAN (repositorio de Miktex) no se sube. Si no, se sube (estilo baobab).

+.tex: contenido de las páginas. Imprescindible -> se sube.

-.aux, .log, .nav, .out, .snm, .toc, .vrb: Makefile: Miktex genera este tipo de archivo de forma automática para crear el ESTILO. Proceso automático -> no se sube.

-.aux, .log, .out: Miktex genera este tipo de archivo de forma automática al compilar el .tex. Proceso automático -> no se sube.

### Python
Se suben los directorios y archivos señalados con +:
+Functions.py: contiene todas las funciones desarrolladas. Imprescindible -> se sube.

+Main.py: scripts principal que llama a las funciones contenidas en Functions. Imprescindible -> se sube.

-.pickle, json: contiene datos. En general, no se suben. Si se quieren compartir Drive o slack.

### R
Se suben los directorios y archivos señalados con +:

+Functions: contiene todas las funciones desarrolladas. Imprescindible -> se sube.

+Main.R: scripts principal que llama a las funciones contenidas en Functions. Imprescindible -> se sube.

-.rds: contiene datos. En general, no subir

-.RData: R, al cerrar sesión, almacena los elementos de entorno en este archivo. No se sube.

-.RHistory: R, al cerrar sesión, almacena los comandos introducidor en la terminal en este archivo. No se sube.

### R Markdown

+.md: depende de su contenido. Se incluyen README.md y documentación en Markdown.
-.md: si se usa Markdown para hacer estudios o reportes personales, no se suben.
-.html/.pdf: Markdown transforma el archivo .md en otro formato de salida, por ejemplo html o pdf, "al igual que Latex convierte .tex en .pdf". No se sube al repositorio



### Cosas para hacer justo antes de hacer commit:

En la ventana de commit, se puede hacer doble click en cada fichero al que se va a incluir en el commit para que git muestre una ventana con la comparación de los ficheros en "antes" y "después". En esta ventana se pueden revertir cambios de manera individual dentro del fichero. También se pueden deshacer todos los cambios en un fichero.

1. Revisar que no se están agregando ficheros **no deseados** (ver *NO agregar ficheros que*).
1. Revisar, en ficheros que no debían haber cambiado, qué ha cambiado y si vale la pena hacer commit a ese fichero o hacer revert. Por ejemplo: `Project.xml` en AIMMS.
1. En los ficheros que sí deben cambiar: ver si hay cambios no-deseados y hacer revert a parte de los cambios del fichero. Por ejemplo: haber cambiado de orden cosas en AIMMS sin querer.
1. Revisar que se están agregando ficheros **nuevos importantes** (ver *SÍ agregar ficheros que*).
