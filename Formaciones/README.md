##Estructura de la carpeta Formaciones
* Nombre proyecto
  * Material formativo
    * Presentacion
    * Caso practico
      * Instalacion
      * Caso Introduccion
      * Caso Resolucion
      * Caso Discusion
  * Material profesor
    * Gui�n
    * Chuleta presentacion
  * Tareas 
    * Iniciales
    * Finales



##VRP
##Estructura del material formativo
* Presentacion
  * .ppt utilizada en la imparticion del curso
  
* Caso practico (AIMMS)
  * AIMMS Instalacion
    * .pdf con instrucciones de instalacion de AIMMS
  * Caso Presentaci�n 
    * Modelo + interfaz AIMMS
  * Caso Resolucion
    * .pdf con los pasos a seguir en la resolucion del caso AIMMS
  * Caso Discusion
    * .pdf explicativo de la solucion del caso planteado
  
  
##Estructura del material del profesor
* Gui�n
  * .pdf explicativo del caso completo
  
* Chuleta presentacion
  * .pdf con los puntos claves que deben tocarse durante la imparticion del curso

  
##Estructura de las tareas
* Tareas iniciales
  * Hay tres archivos .pdf con su correspondiente .xlsx
      * el .pdf contiene la explicacion y los pasos a realizar de la tarea
      * el .xlsx contiene los datos relativos a la tarea
      * se deben mandar los dos archivos (del mismo nombre) conjuntamente al alumno
  * En caso de que se quiera mandar una unica tarea, mandar TI_unica_alum
  * En caso de que se quiera mandar dos tareas, mandar TI_01_alum, y posteriormente TI_02_alum
  
* Tareas finales 