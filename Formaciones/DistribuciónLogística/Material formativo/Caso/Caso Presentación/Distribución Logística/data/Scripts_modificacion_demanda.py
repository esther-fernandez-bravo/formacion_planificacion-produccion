
import geopy as geo
import numpy as np
import pandas as pd
import xlrd as readEx
import copy as cp
from operator import itemgetter

def Generar_coordenadas(NumeroPuntos,CotaSupLat, CotaInfLat, CotaSupLong, CotaInfLong, Pais, NombreExcel):
    '''
    Genera coordenadas de un pais

    param NumeroPuntos (numero): numero de coordenadas a crear
    param CotaSupLat (numero): cota superior de latitud
    param CotaInfLat (numero): cota inferior de latitud
    param CotaSupLong (numero): cota superior de latitud
    param CotaInfLong (numero): cota inferior de latitud
    param Pais (lista): pais o paises a los cuales deben pertenecer las coordenadas
    param csv (NombreExcel): if False no exporta, si no representa el nombre del archivo
    return: pandas dataframe con columnas lat | lon

    require
    geopy as geo: comprobar localizacion de las coordenadas
    numpy as np: generacion de coordenadas aleatorias
    pandas as pd: devuelve un dataframe
    
    '''

    

    CoordenadasSalida = []   
    CountCoordenadas = 0

    while CountCoordenadas != NumeroPuntos:
        LatPropuesta = round(np.random.uniform(CotaSupLat, CotaInfLat),4)
        LonPropuesta = round(np.random.uniform(CotaSupLong, CotaInfLong),4)
        geolocator = geo.Nominatim(timeout=10)
        location = geolocator.reverse('"' + str(LatPropuesta) + ',' +  str(LonPropuesta)+'"')
        if location.address != None:
                Direccion = location.address

                k = Direccion.count(',')
                if k >1:
                    position = []
                    for p in range(0, k):
                        position.append([j for j, x in enumerate(Direccion) if x == ","][p]+1)

                    Info = [None] * (len(position))
                    j = 0
                    Info[j] = Direccion[(position[j] + 1):(position[j + 1] - 1)]
                    for j in range(1, len(position)-1):
                        Info[j] = Direccion[(position[j] + 1):(position[j + 1])-1]

                    j = len(position) - 1
                    Info[j] = Direccion[(position[-1] + 1):(len(Direccion))]
                    if Info[j] in Pais:
                        CoordenadasSalida.append([LatPropuesta,LonPropuesta])
                elif k == 1:
                    position = []
                    position.append([j for j, x in enumerate(Direccion) if x == ","][0]+1)

                    Info = [None] * (len(position))                
                    Info[0] = Direccion[(position[-1] + 1):(len(Direccion))]
                    if Info in Pais:
                        CoordenadasSalida.append([LatPropuesta,LonPropuesta])
                                
                elif k == 0:
                    if Direccion in Pais:
                        CoordenadasSalida.append([LatPropuesta,LonPropuesta])

                CountCoordenadas += 1


    CoordenadasSalida = pd.DataFrame(CoordenadasSalida)
    CoordenadasSalida.columns = ['lat', 'lon']
    if NombreExcel != False:
        writer = pd.ExcelWriter(NombreExcel+'.xlsx', engine='xlsxwriter')
        CoordenadasSalida.to_excel(writer, sheet_name='Coordenadas', index=False)
        writer.save()

    return CoordenadasSalida

def EscribirExcel(df, NombreExcel, NombreHoja):
    '''
    Guarda un dataframe en excel con formato baobab

    param df: pandas dataframe a escribir
    param NombreExcel: nombre del archivo (sin extension)
    param NombreHoja:  nombre de la hoja
    require pandas,

    Font
        'font_name', 'font_size', 'font_color', 'bold', 'italic', underline', 'font_strikeout', 'font_script'
    Number 'num_format'

    Protection
        Lock cells       | 'locked'
        Hide formulas	 | 'hidden'
    Alignment
        Horizontal align | 'align'              Justify last	 | 'text_justlast'
        Vertical align	 | 'valign'             Center across	 | 'center_across'
        Rotation	     | 'rotation'           Indentation	     | 'indent'
        Text wrap	     | 'text_wrap'          Shrink to fit	 | 'shrink'
        Reading order	 | 'reading_order'

    Pattern
        Cell pattern	 | 'pattern'
        Background color | 'bg_color'
        Foreground color | 'fg_color'

    Border
        Cell border	     | 'border'             Border color	 | 'border_color'
        Bottom border	 | 'bottom'             Bottom color     | 'bottom_color'
        Top border	     | 'top'                Top color	     | 'top_color'
        Left border	     | 'left'               Left color       | 'left_color'
        Right border	 | 'right'              Right color	     | 'right_color'
    '''

    # Colores
    MarronBaobab = '#C85A28'
    NaranjaBaobab = '#FF8137'
    Gris = '#808080'

    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(NombreExcel + '.xlsx', engine='xlsxwriter', date_format='mmmm/dd/yyyy',
                            datetime_format='mmm d yyyy hh:mm:ss')

    # Convert the dataframe to an XlsxWriter Excel object.
    df.to_excel(writer, sheet_name=NombreHoja, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets[NombreHoja]

    # Apply a conditional format to the cell range.

    FormatoCabecera = workbook.add_format({'text_wrap': True, 'align': 'center', \
                                           'font_name': 'Century Gothic', 'font_size': 12, 'font_color': 'white',
                                           'bold': True, \
                                           'fg_color': MarronBaobab, 'border': 2, \
                                           'bottom_color': NaranjaBaobab, 'top_color': MarronBaobab,
                                           'left_color': MarronBaobab, 'right_color': MarronBaobab})

    FormatoMediaTablaIzquierda = workbook.add_format({'text_wrap': True, 'valign': 'center',  \
                                                      'font_name': 'Century Gothic', 'font_size': 12,
                                                      'font_color': 'black', 'bold': False, \
                                                      'fg_color': 'white', 'bottom': 1, 'left': 2, 'right': 1, \
                                                      'bottom_color': Gris, 'top_color': Gris,
                                                      'left_color': MarronBaobab, 'right_color': Gris})

    FormatoMediaTablaCentro = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                   'font_name': 'Century Gothic', 'font_size': 12,
                                                   'font_color': 'black', 'bold': False, \
                                                   'fg_color': 'white', 'bottom': 1, 'left': 1, 'right': 1, \
                                                   'bottom_color': Gris, 'top_color': Gris, 'left_color': Gris,
                                                   'right_color': Gris})

    FormatoMediaTablaDerecha = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 1, 'left': 1, 'right': 2, \
                                                    'bottom_color': Gris, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': MarronBaobab})

    FormatoFinalTablaIzquierda = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                      'font_name': 'Century Gothic', 'font_size': 12,
                                                      'font_color': 'black', 'bold': False, \
                                                      'fg_color': 'white', 'bottom': 2, 'left': 2, 'right': 1, \
                                                      'bottom_color': MarronBaobab, 'top_color': Gris,
                                                      'left_color': MarronBaobab, 'right_color': Gris})

    FormatoFinalTablaCentro = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                   'font_name': 'Century Gothic', 'font_size': 12,
                                                   'font_color': 'black', 'bold': False, \
                                                   'fg_color': 'white', 'bottom': 2, 'left': 1, 'right': 1, \
                                                   'bottom_color': MarronBaobab, 'top_color': Gris, 'left_color': Gris,
                                                   'right_color': Gris})

    FormatoFinalTablaDerecha = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 2, 'left': 1, 'right': 2, \
                                                    'bottom_color': MarronBaobab, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': MarronBaobab})

    # Cabecera
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(0, col_num, value, FormatoCabecera)

    # Primeras columnas

    if len(df[df.columns[j]]) ==1:
        j = 0
        for i in range(0, len(df[df.columns[j]]) - 1):
            worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaIzquierda)
    elif len(df[df.columns[j]]) >1:
        j = 0
        for i in range(0, len(df[df.columns[j]]) - 1):
            worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaIzquierda)
        worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaIzquierda)

        # Columnas intermedias
        if len(df.columns) > 2:
            for j in range(1, len(df.columns) - 1):
                for i in range(0, len(df[df.columns[j]]) - 1):
                    worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaCentro)
                worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaCentro)

        # Columnas final
        j = j + 1
        for i in range(0, len(df[df.columns[j]]) - 1):
            worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaDerecha)
        worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaDerecha)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()

def FormatearHoja(df, writer, NombreHoja):
    '''
    Guarda un dataframe en excel con formato baobab

    param df: pandas dataframe a escribir
    param NombreExcel: nombre del archivo (sin extension)
    param NombreHoja:  nombre de la hoja
    require pandas,
    '''

    # Colores
    MarronBaobab = '#C85A28'
    NaranjaBaobab = '#FF8137'
    Gris = '#808080'


    # Convert the dataframe to an XlsxWriter Excel object.
    df.to_excel(writer, sheet_name=NombreHoja, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets[NombreHoja]

    # Apply a conditional format to the cell range.

    FormatoCabecera = workbook.add_format({'text_wrap': True, 'align': 'center', \
                                            'font_name': 'Century Gothic', 'font_size': 12, 'font_color': 'white',
                                            'bold': True, \
                                            'fg_color': MarronBaobab, 'border': 2, \
                                            'bottom_color': NaranjaBaobab, 'top_color': MarronBaobab,
                                            'left_color': MarronBaobab, 'right_color': MarronBaobab})

    FormatoMediaTablaIzquierda = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                        'font_name': 'Century Gothic', 'font_size': 12,
                                                        'font_color': 'black', 'bold': False, \
                                                        'fg_color': 'white', 'bottom': 1, 'left': 2, 'right': 1, \
                                                        'bottom_color': Gris, 'top_color': Gris,
                                                        'left_color': MarronBaobab, 'right_color': Gris})

    FormatoMediaTablaCentro = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 1, 'left': 1, 'right': 1, \
                                                    'bottom_color': Gris, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': Gris})

    FormatoMediaTablaDerecha = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 1, 'left': 1, 'right': 2, \
                                                    'bottom_color': Gris, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': MarronBaobab})

    FormatoFinalTablaIzquierda = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                        'font_name': 'Century Gothic', 'font_size': 12,
                                                        'font_color': 'black', 'bold': False, \
                                                        'fg_color': 'white', 'bottom': 2, 'left': 2, 'right': 1, \
                                                        'bottom_color': MarronBaobab, 'top_color': Gris,
                                                        'left_color': MarronBaobab, 'right_color': Gris})

    FormatoFinalTablaCentro = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 2, 'left': 1, 'right': 1, \
                                                    'bottom_color': MarronBaobab, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': Gris})

    FormatoFinalTablaDerecha = workbook.add_format({'text_wrap': True, 'valign': 'center', \
                                                    'font_name': 'Century Gothic', 'font_size': 12,
                                                    'font_color': 'black', 'bold': False, \
                                                    'fg_color': 'white', 'bottom': 2, 'left': 1, 'right': 2, \
                                                    'bottom_color': MarronBaobab, 'top_color': Gris, 'left_color': Gris,
                                                    'right_color': MarronBaobab})

    # Cabecera
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(0, col_num, value, FormatoCabecera)
    j = 0
    if len(df[df.columns[j]]) == 1:
        i = 0
        worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoFinalTablaIzquierda)
        if len(df.columns) > 2:
            for j in range(1, (len(df.columns)-1)):
                worksheet.write(i + 1, j , df[df.columns[j]][i], FormatoFinalTablaCentro)

        j = j + 1
        worksheet.write(i + 1, j , df[df.columns[j]][i], FormatoFinalTablaDerecha)

        for col_num in enumerate(df.columns.values):
            worksheet.set_column(col_num[0], col_num[0],\
                                    max(len(col_num[1]), max([len(str(s)) for s in df[col_num[1]]])) + 10)

    elif len(df[df.columns[j]]) > 1:
        for i in range(0, len(df[df.columns[j]]) - 1):
            worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaIzquierda)
        worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaIzquierda)

        # Columnas intermedias
        if len(df.columns) > 2:
            for j in range(1, len(df.columns) - 1):
                for i in range(0, len(df[df.columns[j]]) - 1):
                    worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaCentro)
                worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaCentro)

        # Columnas final
        j = j + 1
        for i in range(0, len(df[df.columns[j]]) - 1):
            worksheet.write(i + 1, j, df[df.columns[j]][i], FormatoMediaTablaDerecha)
        worksheet.write(i + 2, j, df[df.columns[j]][i + 1], FormatoFinalTablaDerecha)

        for col_num in enumerate(df.columns.values):
            worksheet.set_column(col_num[0],col_num[0], max( len(col_num[1]), max([len(str(s)) for s in df[col_num[1]]]))+10)

    return writer

def ConvertirExcel(NombreExcelEntrada,NombreExcelSalida):
    '''
    Aplica formato baobab a las tablas de un excel
    param NombreExcelEntrada: excel a editar (con extension)
    param NombreExcelSalida: nombre del archivo formateado(sin extension)

    '''

    xl = pd.ExcelFile(NombreExcelEntrada)
    NombreHoja = xl.sheet_names

    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(NombreExcelSalida + '.xlsx', engine='xlsxwriter', date_format='mmmm/dd/yyyy',
                            datetime_format='mmm d yyyy hh:mm:ss')

    for iHoja in range(0, len (NombreHoja)):
        df = xl.parse(NombreHoja[iHoja])
        writer = FormatearHoja(df, writer, NombreHoja[iHoja])

    writer.save()

def ModificarDemandaMetal(NombreExcelEntrada,NombreHoja,NombreExcelSalida,Variacion,PorcentajeClientes):
    '''
    Modifica la demanda de cliente aleatorios. Suma o resta x veces la desviacion tipica sobre un porcentaje de clientes.

    param NombreExcelEntrada (string): archivo sobre el cual modificar la demanda, con extension.
    param NombreHoja (string): hoja que contiene la demanda.
    param NombreExcelSalida (string): archivo de salida, SIN extension .xlsx.
    param Variacion (numero): numero de veces a sumar la desviacion tipica a la demabda de campa. Si es negativo se resta.
    param PorcentajeClientes (numero): porcentaje de clientes a los cuales modificar la demanda ([0,1]).
    

    return: excel


    require
    
    numpy as np: generacion de coordenadas aleatorias y desviacion tipica
    pandas as pd: devuelve un dataframe
    import xlrd as readEx
    import copy as cp

    '''

    df = pd.read_excel(NombreExcelEntrada, sheetname=NombreHoja)

    Demanda = df.pDemandaClientes.dropna()

    DesviacionDemanda = np.std(Demanda)

    index = []
    Dimension = len(Demanda)
    stop = round(PorcentajeClientes*Dimension/2, 0)


    while len(index) != stop :
        index.append(int(round(np.random.uniform(0, Dimension), 0)))
        index=list(set(index))

    for i in range (0, len(index)):
        df['pDemandaClientes'].iloc[index[i]+1] = cp.deepcopy(df['pDemandaClientes'].iloc[index[i]+1] + Variacion*DesviacionDemanda)

    NuevaDemanda = pd.DataFrame(df,columns = list(df.columns))

    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(NombreExcelSalida + str(PorcentajeClientes) + str(Variacion) + '.xlsx', engine='xlsxwriter', date_format='dd/mm/yyyy',
                            datetime_format='dd/mm/yyyy')

    # Convert the dataframe to an XlsxWriter Excel object.
    NuevaDemanda.to_excel(writer, sheet_name=NombreHoja, index=False)
    writer.save()

#### Ejemplos uso

# NombreExcelEntrada = 'C:/Users/bob_lucia/Desktop/Desktop/Pernod/Esther/formacion_planificacion-produccion/Formaciones/DistribuciónLogística/Material formativo/Caso/Caso Presentación/Distribución Logística/data/Datos_entrada.xlsx'
# NombreHoja = 'demanda'
# NombreExcelSalida = 'C:/Users/bob_lucia/Desktop/Desktop/GasNatural/gas-natural/data/input_prospection/Aumento10Porciento_2_1.xlsx'
# VarDispersionCampa = 2
# VarDispersionCubierto  = 1
# PorcentajeClientes = 0.1
# ModificarDemanda(NombreExcelEntrada,NombreHoja,NombreExcelSalida,VarDispersionCampa,VarDispersionCubierto,PorcentajeClientes)
# 
# 
# 
# NumeroPuntos = 100
# CotaSupLat = -18.2441
# CotaInfLat = -53.7919
# CotaSupLong = -75.3435
# CotaInfLong = -67.3154
# Pais = ['Chile']
# NombreExcel = False
# 
# xl = pd.ExcelFile('C:/Users/bob_lucia/DatosChile.xlsx')
# df = xl.parse('Coordenadas')
# 
# Generar_coordenadas(NumeroPuntos,CotaSupLat, CotaInfLat, CotaSupLong, CotaInfLong, Pais, NombreExcel)
# 
# 
# NombreExcelEntrada = 'C:/Users/bob_lucia/Desktop/Desktop/GasNatural/gas-natural/data/input_prospection/DATOS NATURGY-V3.xlsx'
# NombreHoja = 'Demandas'
# NombreExcelSalida = 'C:/Users/bob_lucia/Desktop/Desktop/Pernod/Esther/formacion_planificacion-produccion/Formaciones/DistribuciónLogística/Material formativo/Caso/Caso Presentación/Distribución Logística/data/Datos_entrada_Optimista2'
# PorcentajeClientes = 0.1
# 
# ModificarDemandaMetal(NombreExcelEntrada,NombreHoja,NombreExcelSalida,Variacion,PorcentajeClientes)




