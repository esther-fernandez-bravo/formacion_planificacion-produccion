Esta carpeta contiene tareas que pueden ser envíadas a los asistentes antes del comienzo del curso.
Si se envía una única tarea, se sugiere enviar TI_00.
Si se envían varías tareas, se sugiere enviar las tareas en TI_0i en orden.